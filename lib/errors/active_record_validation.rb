# frozen_string_literal: true

class Errors::ActiveRecordValidation
  attr_reader :record

  def initialize(record)
    @record = record
  end

  def errors
    resource = record.class.to_s.underscore
    record.errors.to_hash.map do |field, message|
      {
        resource: resource,
        field: field,
        message: message.first
      }
    end
  end
end
