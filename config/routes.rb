Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  devise_for :users, defaults: { format: :json }, path: 'api/v1',
    path_names: {
      sign_in: 'login',
      sign_out: 'logout',
      registration: 'signup'
    },
    controllers: {
      sessions: 'api/v1/users/sessions',
      registrations: 'api/v1/users/registrations'
    }

  namespace :api do
    namespace :v1, constraints: { format: 'json' } do
      resources :users, only: %i[index show update destroy]

      match '*path', to: 'errors#router_not_found', via: :all
    end
  end
end
