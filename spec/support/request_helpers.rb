# frozen_string_literal: true

require 'devise/jwt/test_helpers'

include Warden::Test::Helpers

module RequestHelpers
  def bearer_token(user)
    headers = { 'Accept' => 'application/json', 'Content-Type' => 'application/json' }
    auth_headers = Devise::JWT::TestHelpers.auth_headers(headers, user)
    auth_headers['Authorization']
  end
end
