# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    password { 123456 }
    password_confirmation { 123456 }
    first_name { Faker::Name.name.first }
    last_name { Faker::Name.name.last }
    role { 'normal' }
    jti { SecureRandom.uuid }

    factory :user_full_access do
      role { 'full_access' }
    end
  end
end
