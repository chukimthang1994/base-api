# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'Api::V1::Users::RegistrationsController', type: :request do
  describe 'Users API' do
    path '/api/v1/signup' do
      let(:user) do
        {
          user: {
            email: Faker::Internet.email,
            password: '123456',
            password_confirmation: '123456',
            first_name: Faker::Name.name.first,
            last_name: Faker::Name.name.last
          }
        }
      end

      post 'Create a user' do
        tags 'User'
        consumes 'application/json'
        produces 'application/json'
        parameter name: 'user', in: :body, schema: {
          type: :object,
          properties: {
            user: {
              type: :object,
              properties: {
                email: { type: :string },
                password: { type: :string },
                password_confirmation: { type: :string },
                first_name: { type: :string },
                last_name: { type: :string },
                birthday: { type: :string },
                sex: { type: :integer }
              }
            }
          },
          required: %w[email password password_confirmation first_name last_name]
        }

        response '200', 'Successfully' do
          schema type: :object, properties: {
            user: {
              type: :object,
              properties: {
                id: { type: :integer },
                email: { type: :string },
                first_name: { type: :string },
                last_name: { type: :string },
                birthday: { type: :string },
                sex: { type: :integer }
              }
            }
          }

          run_test! do |response|
            data = JSON.parse(response.body)
            expect(data['success']).to eq(true)
            expect(response.status).to eq(200)
            expect(data['data']['user']['email']).to eq(user[:user][:email])
          end
        end
      end
    end
  end
end
