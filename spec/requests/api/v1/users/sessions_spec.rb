# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'Api::V1::Users::SessionsController', type: :request do
  describe 'Users API' do
    path '/api/v1/login' do
      post 'Login user' do
        tags 'User'
        consumes 'application/json'
        produces 'application/json'
        parameter name: :user, in: :body, schema: {
          type: :object,
          properties: {
            email: { type: :string },
            password: { type: :string }
          },
          required: %w[email password]
        }

        response '200', 'Successfully' do
          let(:user) { create(:user) }

          before do
            sign_in(user)
          end

          run_test! do |response|
            data = JSON.parse(response.body)
            expect(response).to have_http_status(:ok)
            expect(response.headers['Authorization']).to be_present
          end
        end

        response '401', 'Unauthenticated' do
          let(:user) { attributes_for :user, password: nil }
          run_test! do |response|
            expect(response).to have_http_status(:unauthorized)
          end
        end
      end
    end

    path '/api/v1/logout' do
      delete 'Logout user' do
        tags 'User'
        security [bearerAuth: []]
        let(:user) { create(:user) }

        before do
          sign_in(user)
        end

        response '200', 'Successfully' do
          let(:Authorization) { bearer_token(user) }

          run_test! do |response|
            expect(response).to have_http_status(:ok)
          end
        end
      end
    end
  end
end
