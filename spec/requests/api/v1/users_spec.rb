# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'Api::V1::UsersController', type: :request do
  describe 'Users API with role: full_access' do
    path '/api/v1/users' do
      let(:user_full_access) { create(:user_full_access) }
      let(:Authorization) { "Bearer #{::Base64.strict_encode64('jsmith:jspass')}" }

      before do
        sign_in(user_full_access)
      end

      get 'List user' do
        tags 'Users'
        security [bearerAuth: []]
        produces 'application/json'
        parameter name: :page, in: :query, type: :integer, required: false
        parameter name: :per_page, in: :query, type: :integer, required: false

        response '200', 'Successfully' do
          schema type: :object, properties: {
            success: {
              type: :boolean
            },
            data: {
              type: :object,
              properties: {
                users: {
                  type: :array,
                  items: {
                    type: :object,
                    properties: {
                      id: { type: :integer },
                      email: { type: :string },
                      first_name: { type: :string },
                      last_name: { type: :string },
                      sex: { type: :integer },
                      birthday: { type: :date },
                      role: { type: :string }
                    }
                  }
                },
                pagination: {
                  type: :object,
                  properties: {
                    total_count: { type: :integer },
                    limit_value: { type: :integer },
                    total_pages: { type: :integer },
                    current_page: { type: :integer }
                  }
                }
              }
            }
          }

          before do
            create_list(:user, 3)
          end

          let(:page) { '1' }
          let(:per_page) { '10' }

          run_test! do |response|
            result = JSON.parse(response.body)
            expect(response).to have_http_status(:ok)
            expect(result['data']['users']).to be_present
            expect(result['data']['pagination']['current_page']).to eql(1)
          end
        end
      end
    end

    path '/api/v1/users/{id}' do
      let(:user_full_access) { create(:user_full_access) }
      let(:user) { create(:user) }
      let(:Authorization) { "Bearer #{::Base64.strict_encode64('jsmith:jspass')}" }

      before do
        sign_in(user_full_access)
      end

      get 'Show user detail' do
        tags 'Users'
        security [bearerAuth: []]
        produces 'application/json'
        parameter name: :id, in: :path, type: :string

        response '200', 'Successfully' do
          schema type: :object, properties: {
            success: {
              type: :boolean
            },
            data: {
              type: :object,
              properties: {
                user: {
                  type: :object,
                  properties: {
                    id: { type: :integer },
                    email: { type: :string },
                    first_name: { type: :string },
                    last_name: { type: :string },
                    sex: { type: :integer },
                    birthday: { type: :date },
                    role: { type: :string }
                  }
                }
              }
            }
          }

          let(:id) { user.id }

          run_test! do |response|
            result = JSON.parse(response.body)
            expect(response).to have_http_status(:ok)
            expect(result['data']['user']['id']).to eql(id)
          end
        end

        response '404', 'Not Found' do
          let(:id) { 'invalid' }

          run_test! do |response|
            expect(response).to have_http_status(:not_found)
          end
        end
      end

      patch 'Update user' do
        let(:full_name) {  Faker::Name.name }
        let(:user_params) do
          {
            user: {
              first_name: full_name.first,
              last_name: full_name.last
            }
          }
        end

        security [bearerAuth: []]
        tags 'Users'
        consumes 'application/json'
        produces 'application/json'
        parameter name: :id, in: :path, type: :string
        parameter name: :user_params, in: :body, schema: {
          type: :object,
          properties: {
            user: {
              type: :object,
              properties: {
                email: { type: :string },
                password: { type: :string },
                password_confirmation: { type: :string },
                first_name: { type: :string },
                last_name: { type: :string },
                birthday: { type: :string },
                sex: { type: :integer }
              }
            }
          }
        }

        response '200', 'Successfully' do
          schema type: :object, properties: {
            success: {
              type: :boolean
            },
            data: {
              type: :object,
              properties: {
                user: {
                  type: :object,
                  properties: {
                    id: { type: :integer },
                    email: { type: :string },
                    first_name: { type: :string },
                    last_name: { type: :string },
                    sex: { type: :integer },
                    birthday: { type: :date },
                    role: { type: :string }
                  }
                }
              }
            }
          }

          let(:id) { user.id }

          run_test! do |response|
            result = JSON.parse(response.body)
            expect(response).to have_http_status(:ok)
            expect(result['data']['user']['first_name']).to eql(full_name.first)
            expect(result['data']['user']['last_name']).to eql(full_name.last)
          end
        end

        response '404', 'Not Found' do
          let(:id) { 'invalid' }

          run_test! do |response|
            expect(response).to have_http_status(:not_found)
          end
        end

        response '422', 'Invalid inputs' do
          let(:id) { user.id }

          before do
            user_params[:user][:email] = Faker::Name.name
          end

          run_test! do |response|
            expect(response).to have_http_status(:unprocessable_entity)
          end
        end
      end

      delete 'Destroy user' do
        tags 'Users'
        security [bearerAuth: []]
        produces 'application/json'
        parameter name: :id, in: :path, type: :string

        response "200", "Successfully" do
          let(:id) { user.id }
 
          run_test! do |response|
            expect(response).to have_http_status(:ok)
          end
        end

        response '404', 'Not Found' do
          let(:id) { 'invalid' }

          run_test! do |response|
            expect(response).to have_http_status(:not_found)
          end
        end
      end
    end
  end

  describe 'Users API with role: normal' do
    path '/api/v1/users' do
      let(:user) { create(:user) }
      let(:Authorization) { "Bearer #{::Base64.strict_encode64('jsmith:jspass')}" }

      before do
        sign_in(user)
      end

      get 'List user' do
        tags 'Users'
        security [bearerAuth: []]
        produces 'application/json'
        parameter name: :page, in: :query, type: :integer, required: false
        parameter name: :per_page, in: :query, type: :integer, required: false

        response '401', 'Unauthorized' do
          before do
            create_list(:user, 3)
          end

          let(:page) { '1' }
          let(:per_page) { '10' }

          run_test! do |response|
            expect(response).to have_http_status(:unauthorized)
          end
        end
      end
    end

    path '/api/v1/users/{id}' do
      let(:user) { create(:user) }
      let(:user_other) { create(:user) }
      let(:Authorization) { "Bearer #{::Base64.strict_encode64('jsmith:jspass')}" }

      before do
        sign_in(user)
      end

      get 'Show user detail' do
        tags 'Users'
        security [bearerAuth: []]
        produces 'application/json'
        parameter name: :id, in: :path, type: :string
        parameter name: 'user', in: :body, schema: {
          type: :object,
          properties: {
            user: {
              type: :object,
              properties: {
                email: { type: :string },
                password: { type: :string },
                password_confirmation: { type: :string },
                first_name: { type: :string },
                last_name: { type: :string },
                birthday: { type: :string },
                sex: { type: :integer }
              }
            }
          }
        }
        response '200', 'Successfully' do
          schema type: :object, properties: {
            success: {
              type: :boolean
            },
            data: {
              type: :object,
              properties: {
                user: {
                  type: :object,
                  properties: {
                    id: { type: :integer },
                    email: { type: :string },
                    first_name: { type: :string },
                    last_name: { type: :string },
                    sex: { type: :integer },
                    birthday: { type: :date },
                    role: { type: :string }
                  }
                }
              }
            }
          }

          let(:id) { user.id }

          run_test! do |response|
            result = JSON.parse(response.body)
            expect(response).to have_http_status(:ok)
            expect(result['data']['user']['id']).to eql(id)
          end
        end

        response '404', 'Not Found' do
          let(:id) { 'invalid' }

          run_test! do |response|
            expect(response).to have_http_status(:not_found)
          end
        end

        response '401', 'Unauthorized' do
          let(:id) { user_other.id }

          run_test! do |response|
            expect(response).to have_http_status(:unauthorized)
          end
        end
      end

      patch 'Update user' do
        let(:id) { user.id }
        let(:full_name) {  Faker::Name.name }
        let(:user_params) do
          {
            user: {
              first_name: full_name.first,
              last_name: full_name.last
            }
          }
        end

        security [bearerAuth: []]
        tags 'Users'
        consumes 'application/json'
        produces 'application/json'
        parameter name: :id, in: :path, type: :string
        parameter name: :user_params, in: :body, schema: {
          type: :object,
          properties: {
            user: {
              type: :object,
              properties: {
                email: { type: :string },
                password: { type: :string },
                password_confirmation: { type: :string },
                first_name: { type: :string },
                last_name: { type: :string },
                birthday: { type: :string },
                sex: { type: :integer }
              }
            }
          }
        }

        response '200', 'Successfully' do
          schema type: :object, properties: {
            success: {
              type: :boolean
            },
            data: {
              type: :object,
              properties: {
                user: {
                  type: :object,
                  properties: {
                    id: { type: :integer },
                    email: { type: :string },
                    first_name: { type: :string },
                    last_name: { type: :string },
                    sex: { type: :integer },
                    birthday: { type: :date },
                    role: { type: :string }
                  }
                }
              }
            }
          }

          let(:id) { user.id }

          run_test! do |response|
            result = JSON.parse(response.body)
            expect(response).to have_http_status(:ok)
            expect(result['data']['user']['first_name']).to eql(full_name.first)
            expect(result['data']['user']['last_name']).to eql(full_name.last)
          end
        end

        response '404', 'Not Found' do
          let(:id) { 'invalid' }

          run_test! do |response|
            expect(response).to have_http_status(:not_found)
          end
        end

        response '422', 'Invalid inputs' do
          let(:id) { user.id }

          before do
            user_params[:user][:email] = Faker::Name.name
          end

          run_test! do |response|
            expect(response).to have_http_status(:unprocessable_entity)
          end
        end

        response '401', 'Unauthorized' do
          let(:id) { user_other.id }

          run_test! do |response|
            expect(response).to have_http_status(:unauthorized)
          end
        end
      end

      delete 'Destroy user' do
        tags 'Users'
        security [bearerAuth: []]
        produces 'application/json'
        parameter name: :id, in: :path, type: :string

        response '401', 'Unauthorized' do
          let(:id) { user.id }

          run_test! do |response|
            expect(response).to have_http_status(:unauthorized)
          end
        end
      end
    end
  end
end
