# frozen_string_literal: true

class UserJwtDenylist < ApplicationRecord
  include Devise::JWT::RevocationStrategies::Denylist

  self.table_name = 'user_jwt_denylist'
end
