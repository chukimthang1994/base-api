# frozen_string_literal: true

class User < ApplicationRecord
  include Devise::JWT::RevocationStrategies::JTIMatcher

  SEXES = {
    female: 0,
    male: 1,
    other: 2
  }.freeze

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :jwt_authenticatable, jwt_revocation_strategy: UserJwtDenylist

  self.skip_session_storage = %i[http_auth params_auth]

  enum role: { normal: 0, full_access: 1 }

  validates :first_name, :last_name, presence: true
  validates :email, presence: true, uniqueness: true
  validates :sex, inclusion: { in: SEXES.values }
end
