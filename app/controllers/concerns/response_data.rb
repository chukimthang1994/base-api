# frozen_string_literal: true

module ResponseData
  extend ActiveSupport::Concern

  def render_error(status, error_message)
    render json: { success: false, error_message: error_message }, status: status
  end

  def render_data(data = nil)
    render json: { success: true, data: data }, status: :ok
  end

  def pagination(data)
    {
      total_count: data.total_count,
      limit_value: data.limit_value,
      total_pages: data.total_pages,
      current_page: data.current_page
    }
  end
end
