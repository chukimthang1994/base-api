# frozen_string_literal: true

module CommonError
  extend ActiveSupport::Concern

  def self.included(clazz)
    clazz.class_eval do
      rescue_from Exception, with: :render500
      rescue_from ActionController::BadRequest, ActionController::ParameterMissing, with: :render400
      rescue_from Pundit::NotAuthorizedError, with: :render401
      rescue_from ActiveRecord::RecordNotFound, with: :render404
      rescue_from ActiveRecord::RecordInvalid, with: :render422
    end
  end

  private

  def render400(exception = nil)
    render_error(:bad_request, exception&.message)
  end

  def render401(exception = nil)
    render_error(:unauthorized, exception&.message)
  end

  def render404(exception = nil)
    render_error(:not_found, exception&.message)
  end

  def render422(exception = nil)
    render_error(:unprocessable_entity, Errors::ActiveRecordValidation.new(exception.record).errors)
  end

  def render500(exception = nil)
    render_error(:internal_server_error, exception&.message)
  end

  def render401_with_message(message = nil)
    render_error(:unauthorized, message)
  end
end
