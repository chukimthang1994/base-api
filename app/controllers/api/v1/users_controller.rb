# frozen_string_literal: true

class Api::V1::UsersController < Api::V1::BaseController
  before_action :set_user, only: %i[show update destroy]

  # GET /api/v1/users
  def index
    authorize User
    @users = User.order(created_at: :desc).page(params[:page]).per(PER_PAGE)

    render_data({ users: @users, pagination: pagination(@users) })
  end

  # GET /api/v1/users/:id
  def show
    render_data({ user: @user })
  end

  # PATCH /api/v1/users/:id
  def update
    @user.update!(user_params)

    render_data({ user: @user })
  end

  # DELETE /api/v1/users/:id
  def destroy
    @user.destroy!

    render_data({ message: 'Successfully' })
  end

  private

  def set_user
    @user = User.find(params[:id])
    authorize @user
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :first_name,
                                 :last_name, :birthday, :sex)
  end
end
