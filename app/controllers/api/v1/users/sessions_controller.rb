# frozen_string_literal: true

class Api::V1::Users::SessionsController < Devise::SessionsController
  include CommonError
  include ResponseData

  skip_before_action :verify_signed_out_user, only: :destroy
  wrap_parameters :user
  respond_to :json

  # POST /api/v1/login
  def create
    self.resource = warden.authenticate!(auth_options)
    sign_in(resource_name, resource)

    render_data({ message: 'Successfully' })
  end

  # DELETE /api/v1/logout
  def destroy
    return render401_with_message('Unauthorized') unless user_signed_in?

    Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)

    render_data({ message: 'Successfully' })
  end
end
