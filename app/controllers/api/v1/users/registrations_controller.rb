# frozen_string_literal: true

class Api::V1::Users::RegistrationsController < Devise::RegistrationsController
  include CommonError
  include ResponseData

  # POST /api/v1/signup
  def create
    build_resource(user_params)
    resource.save!

    render_data({ user: resource })
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :first_name,
                                 :last_name, :birthday, :sex)
  end
end
