# frozen_string_literal: true

class Api::V1::ErrorsController < Api::V1::BaseController
  def router_not_found
    render_error(:not_found, 'Router not found')
  end
end
