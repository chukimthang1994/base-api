# frozen_string_literal: true

class Api::V1::BaseController < ApplicationController
  include CommonError
  include ResponseData

  before_action :authenticate_user!

  private

  def authenticate_user!
    return render401_with_message('Unauthorized') unless user_signed_in?

    super
  end
end
