# frozen_string_literal: true

class UserPolicy < ApplicationPolicy
  attr_reader :current_user, :user

  def initialize(current_user, user)
    super
    @current_user = current_user
    @user = user
  end

  def index?
    current_user.full_access?
  end

  def show?
    current_user.full_access? || current_user.eql?(@user)
  end

  def update?
    current_user.full_access? || current_user.eql?(@user)
  end

  def destroy?
    current_user.full_access?
  end
end
