# README

#### 1. Version
* Ruby 3.2.2
* Rails 7.0.5

#### 2. Running on local with Docker
* Add .env file to the app root path
* Build docker image
    ```
    docker-compose build
    ```
* Run container
    ```
    docker-compose up
    ```
* Create database
    ```
    docker-compose run app bundle exec rails db:create db:migrate db:seed
    ```

* Other useful command
    ```
    docker attach my_container_name
    docker-compose run app bundle exec rails c
    docker exec -it my_container_name bash
    ```

#### 3. Rubocop
* Check rubocop
    ```
    bundle exec rubocop
    ```
* Fix rubocop
    ```
    bundle exec rubocop -a
    ```

#### 4. Build swagger file
* Run rspec
    ```
    bundle exec rspec
    ```
* General swagger yaml
    ```
    rake rswag:specs:swaggerize
    ```
